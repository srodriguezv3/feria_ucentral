package com.ucentral.sessionbeans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ejb.Stateless;

import com.ucentral.model.CriterioEvaluacion;
import com.ucentral.model.Evaluador;
import com.ucentral.model.ProyectoPrueba;
import com.ucentral.remotebeans.UsuarioEvaluacionRemote;


@Stateless
public class UsuarioEvaluacionBean implements UsuarioEvaluacionRemote, Serializable {


	@Override
	public boolean probarEJB(String mensaje) {
		if (mensaje.equals("saludo")) {
			System.out.println("Hecho");
			return true;
		} else {
			System.out.println("No Hecho");
			return false;
		}
	}

	@Override
	public ArrayList<ProyectoPrueba> consultarProyectosAsignados() {
		Evaluador evaluador = new Evaluador();
		return evaluador.consultarProyectosAsignados();
	}

	@Override
	public boolean calificarProyectos(ProyectoPrueba obj) {
		Evaluador evaluador = new Evaluador();
		return evaluador.calificarProyectos(obj);
	}

	@Override
	public boolean crearCriterioNuevo(CriterioEvaluacion criterio) {
		CriterioEvaluacion nuevoCriterio = new CriterioEvaluacion();
		try {
			return nuevoCriterio.crearCriterioNuevo(criterio);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
}

package com.ucentral.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucentral.bd.ConexionBD;
import com.ucentral.model.ProyectoPrueba;


public class EvaluadorDAO {

	private PreparedStatement objPreparedStatement;
	private ResultSet objResultSet;
	private Connection objConexion;

	// Metodo para consultar los proyectos asignados

	public ArrayList<ProyectoPrueba> consultarProyectosAsignados() throws SQLException {

		objPreparedStatement = null;
		objResultSet = null;
		objConexion = null;

		ArrayList<ProyectoPrueba> lstProyectosAsignados = new ArrayList<ProyectoPrueba>();

		String sqlQuery = "SELECT * FROM feria_ucentral.proyecto where idEvaluador = 1000";

		try {

			objConexion = ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlQuery);
			objResultSet = objPreparedStatement.executeQuery();

			while (objResultSet.next()) {

				ProyectoPrueba proyectosAsignadosBD =new ProyectoPrueba(objResultSet.getInt(1),objResultSet.getString(2),
						objResultSet.getInt(3),objResultSet.getString(4));

				lstProyectosAsignados.add(proyectosAsignadosBD);

			}
			objPreparedStatement.close();
			objConexion.close();

		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();

		} finally {

			if (objConexion != null)
				objConexion.close();

			if (objPreparedStatement != null)
				objPreparedStatement.close();

		}
		return lstProyectosAsignados;
	}
	
	public boolean calificarProyectos (ProyectoPrueba obj) throws SQLException{
		
		objPreparedStatement =  null;
		objResultSet = null;
		objConexion = null;
		int reModify = 0;
		
		String sqlModify = "UPDATE feria_ucentral.proyecto SET nota = ? where idProyecto = ?";
		
		try {
			objConexion =ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlModify);
			
			objPreparedStatement.setInt(1, obj.getNota());
			objPreparedStatement.setInt(2, obj.getIdProyecto());
			
			reModify = objPreparedStatement.executeUpdate();
			objPreparedStatement.close();
			objConexion.close();
			
			if (reModify>0) {
				return true;
			}
			else {
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		
		}finally {
			if (objConexion !=null)
				objConexion.close();
			
			if(objPreparedStatement !=null)
				objPreparedStatement.close();
		}
		
	}

}

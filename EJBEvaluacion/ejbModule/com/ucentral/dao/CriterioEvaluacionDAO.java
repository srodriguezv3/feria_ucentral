package com.ucentral.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ucentral.bd.ConexionBD;
import com.ucentral.model.CriterioEvaluacion;


public class CriterioEvaluacionDAO {

	private PreparedStatement objPreparedStatement;
	private ResultSet objResultSet;
	private Connection objConexion;

	public boolean crearCriterioNuevo(CriterioEvaluacion criterio) throws SQLException {

		objPreparedStatement = null;
		objResultSet = null;
		objConexion = null;
		int reInsert = 0;

		String sqlInsert = "INSERT INTO criteriosEvaluacion (idCriterioEvaluacion, descripcionCriterio, notaCriterio) "
				+ "VALUES (?,?,?)";
		try {
			objConexion = ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlInsert);

			objPreparedStatement.setString(1, criterio.getIdCriterioEvaluacion());
			objPreparedStatement.setString(2, criterio.getDescripcionCriterio());
			objPreparedStatement.setInt(3, criterio.getNotaCriterio());

			reInsert = objPreparedStatement.executeUpdate();

			objPreparedStatement.close();
			objConexion.close();

			if (reInsert > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
			e.getMessage();

		} finally {
			if (objConexion != null)
				objConexion.close();

			if (objPreparedStatement != null)
				objPreparedStatement.close();
		}
		return false;
	}
}
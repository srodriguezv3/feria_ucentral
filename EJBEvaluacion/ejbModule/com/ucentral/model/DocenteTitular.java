package com.ucentral.model;

import java.io.Serializable;

public class DocenteTitular  extends Evaluador implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8133340840143933983L;
	private String idProfesor;
	private String nombresPro;
	private String apellidosPro;
	private boolean cargo;
	private String coreoProfesor;
	private int numeroContacto1;
	private int numeroContacto2;


	
	public DocenteTitular(String idProfesor, String nombresPro, String apellidosPro, boolean cargo,
			String coreoProfesor, int numeroContacto1, int numeroContacto2) {
		super();
		this.idProfesor = idProfesor;
		this.nombresPro = nombresPro;
		this.apellidosPro = apellidosPro;
		this.cargo = cargo;
		this.coreoProfesor = coreoProfesor;
		this.numeroContacto1 = numeroContacto1;
		this.numeroContacto2 = numeroContacto2;
	}


	public String getIdProfesor() {
		return idProfesor;
	}

	public void setIdProfesor(String idProfesor) {
		this.idProfesor = idProfesor;
	}

	public String getNombresPro() {
		return nombresPro;
	}

	public void setNombresPro(String nombresPro) {
		this.nombresPro = nombresPro;
	}

	public String getApellidosPro() {
		return apellidosPro;
	}

	public void setApellidosPro(String apellidosPro) {
		this.apellidosPro = apellidosPro;
	}

	public boolean isCargo() {
		return cargo;
	}

	public void setCargo(boolean cargo) {
		this.cargo = cargo;
	}

	public String getCoreoProfesor() {
		return coreoProfesor;
	}

	public void setCoreoProfesor(String coreoProfesor) {
		this.coreoProfesor = coreoProfesor;
	}

	public int getNumeroContacto1() {
		return numeroContacto1;
	}

	public void setNumeroContacto1(int numeroContacto1) {
		this.numeroContacto1 = numeroContacto1;
	}

	public int getNumeroContacto2() {
		return numeroContacto2;
	}

	public void setNumeroContacto2(int numeroContacto2) {
		this.numeroContacto2 = numeroContacto2;
	}

	public void registrarProyecto() {

	}

	public void modificarDatosProyecto() {

	}

	public void eliminarProyecto() {

	}
	
	

}

package com.ucentral.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.ucentral.dao.EvaluadorDAO;

public class Evaluador implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8395722954626967687L;
	private String idEvaluador;

	public Evaluador(String id_Evaluador, String contraseņa_Evaluador) {
		super();
		this.idEvaluador = id_Evaluador;
	}

	public Evaluador() {
		// TODO Auto-generated constructor stub
	}

	public String getIdEvaluador() {
		return idEvaluador;
	}

	public void setIdEvaluador(String id_Evaluador) {
		this.idEvaluador = id_Evaluador;
	}

	public void evaluarProyecto() {

	}

	public ArrayList<ProyectoPrueba> consultarProyectosAsignados() {
		EvaluadorDAO dao = new EvaluadorDAO();
		
		ProyectoPrueba proyectosAsignados = new ProyectoPrueba();
		return proyectosAsignados.consultarProyectosAsignados();
		
		
	}

	public boolean calificarProyectos(ProyectoPrueba objModel) {
		
		ProyectoPrueba proyectoAcalificar = new ProyectoPrueba();
		return proyectoAcalificar.agregarCalificacionProyecto(objModel);

	}

	public void modificarCalificacion() {

	}

}

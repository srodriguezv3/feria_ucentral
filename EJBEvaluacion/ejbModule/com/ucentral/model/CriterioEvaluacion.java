package com.ucentral.model;

import java.io.Serializable;
import java.sql.SQLException;

import com.ucentral.dao.CriterioEvaluacionDAO;



public class CriterioEvaluacion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6955516262604810011L;
	private String idCriterioEvaluacion;
	private String descripcionCriterio;
	private int notaCriterio;
	
	
	
	public CriterioEvaluacion(String idCriterioEvaluacion, String descripcionCriterio, int notaCriterio) {
		super();
		this.idCriterioEvaluacion = idCriterioEvaluacion;
		this.descripcionCriterio = descripcionCriterio;
		this.notaCriterio = notaCriterio;
	}
	
	public CriterioEvaluacion() {}
	
	
	public String getIdCriterioEvaluacion() {
		return idCriterioEvaluacion;
	}

	public void setIdCriterioEvaluacion(String idCriterioEvaluacion) {
		this.idCriterioEvaluacion = idCriterioEvaluacion;
	}

	public String getDescripcionCriterio() {
		return descripcionCriterio;
	}

	public void setDescripcionCriterio(String descripcionCriterio) {
		this.descripcionCriterio = descripcionCriterio;
	}
	

	public int getNotaCriterio() {
		return notaCriterio;
	}

	public void setNotaCriterio(int notaCriterio) {
		this.notaCriterio = notaCriterio;
	}

	public boolean crearCriterioNuevo(CriterioEvaluacion criterio) throws SQLException {
		CriterioEvaluacionDAO nuevoCriterio = new CriterioEvaluacionDAO();
		return nuevoCriterio.crearCriterioNuevo(criterio);
	}
	
	
	
	
	

}
package com.ucentral.bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;




public class ConexionBD {

private static Connection conexion;
private static ConexionBD objConexionBD;

public static Connection obtenerConexionBaseDeDatos() throws SQLException{
	try {
		Class.forName(com.ucentral.utility.DCMGuia.DRIVER_BD);
		conexion = DriverManager.getConnection(com.ucentral.utility.DCMGuia.CONEXION_BD,com.ucentral.utility.DCMGuia.USUARIO_BD,com.ucentral.utility.DCMGuia.PASSWORD_BD);
		System.out.println("Conectando.......");
		} catch (Exception e) {
		conexion.close();
		e.printStackTrace();
		}
	return conexion; 
}

}
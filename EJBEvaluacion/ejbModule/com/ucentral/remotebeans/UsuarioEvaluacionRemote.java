package com.ucentral.remotebeans;

import java.util.ArrayList;

import javax.ejb.Remote;

import com.ucentral.model.CriterioEvaluacion;
import com.ucentral.model.ProyectoPrueba;



@Remote
public interface UsuarioEvaluacionRemote {
	
	public boolean probarEJB (String mensaje);
	
	public ArrayList<ProyectoPrueba> consultarProyectosAsignados();
	
	public boolean calificarProyectos(ProyectoPrueba obj);
	
	public boolean crearCriterioNuevo(CriterioEvaluacion criterio);

}

package com.ucentral.remotebeans;
import java.util.ArrayList;

import javax.ejb.Remote;

import com.ucentral.model.Proyecto;



@Remote
public interface UsuarioRemote {
	
	
	public boolean probarEJB (String mensaje);
	
	public boolean registrarProyectos(Proyecto obj);
	
	public boolean modificarProyectos(Proyecto obj);
	
	public boolean eliminarProyectos (Proyecto obj);
	
	public ArrayList<Proyecto> consultarProyectos();
	
	
}
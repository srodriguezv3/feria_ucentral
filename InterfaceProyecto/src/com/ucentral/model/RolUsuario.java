package com.ucentral.model;

public class RolUsuario {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4481368782491816563L;
	private int idRol;
	private String nombreRol;
	public RolUsuario(int idRol, String nombreRol) {
		this.idRol = idRol;
		this.nombreRol = nombreRol;
	}
	public int getIdRol() {
		return idRol;
	}
	public void setIdRol(int idRol) {
		this.idRol = idRol;
	}
	public String getNombreRol() {
		return nombreRol;
	}
	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}
	
	
	
}


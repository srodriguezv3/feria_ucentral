package com.ucentral.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;



public class Coordinador implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8527373168309530184L;
		private int idCoordinador;
		private String nombreCoordinador;
		private String correoCoordinador;
		public Coordinador(int idCoordinador, String nombreCoordinador) {
			this.idCoordinador = idCoordinador;
			this.nombreCoordinador = nombreCoordinador;
			
		}
		
		public int getIdCoordinador() {
			return idCoordinador;
		}
		public void setIdCoordinador(int idCoordinador) {
			this.idCoordinador = idCoordinador;
		}
		public String getNombreCoordinador() {
			return nombreCoordinador;
		}
		public void setNombreCoordinador(String nombreCoordinador) {
			this.nombreCoordinador = nombreCoordinador;
		}
		public String getCorreoCoordinador() {
			return correoCoordinador;
		}
		public void setCorreoCoordinador(String correoCoordinador) {
			this.correoCoordinador = correoCoordinador;
		}


}




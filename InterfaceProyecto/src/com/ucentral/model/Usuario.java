package com.ucentral.model;

import java.io.Serializable;

public class Usuario implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5503133307177594405L;
	private int idUsuario;
	private String contraseñaUsuario;
	private String nombreUsuario;
	private String apellidoUsuario;
	private String correoUsuario;
	RolUsuario rol = new RolUsuario(idUsuario, apellidoUsuario);
	
	public Usuario() {}
	
	public Usuario(int idUsuario, String contraseñaUsuario, String nombreUsuario, String apellidoUsuario,
			String correoUsuario, RolUsuario rol) {
		this.idUsuario = idUsuario;
		this.contraseñaUsuario = contraseñaUsuario;
		this.nombreUsuario = nombreUsuario;
		this.apellidoUsuario = apellidoUsuario;
		this.correoUsuario = correoUsuario;
		this.rol = rol;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getContraseñaUsuario() {
		return contraseñaUsuario;
	}
	public void setContraseñaUsuario(String contraseñaUsuario) {
		this.contraseñaUsuario = contraseñaUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getApellidoUsuario() {
		return apellidoUsuario;
	}
	public void setApellidoUsuario(String apellidoUsuario) {
		this.apellidoUsuario = apellidoUsuario;
	}
	public String getCorreoUsuario() {
		return correoUsuario;
	}
	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}
	public RolUsuario getRol() {
		return rol;
	}
	public void setRol(RolUsuario rol) {
		this.rol = rol;
	}
	
	public boolean iniciarSesion(String iniciarUsuario) {
		return true;
	}
	
	public boolean evaluarProyecto(String evaluarProyecto) {
		return true;
	}
	
	public boolean modificarContraseña(String modificarContraseña) {
		return true;
	}
	public boolean recuperarContraseña(String recuperarContraseña) {
		return true;
	}
	
	public boolean cambiarFoto(String cambiarFoto) {
		return true;
	}
	
	public boolean importarFoto(String importarFoto) {
		return true;
	}
	
	public boolean consultarProyectosAsignados(String consultarProyectosAsignados) {
		return true;
	}
	
	public boolean calificarProyectos(String calificarProyectos) {
		return true;
	}

	public boolean modificarProyectos(String modificarProyectos) {
		return true;
	}
}

package com.ucentral.model;

import java.io.Serializable;

public class EspacioFisico implements Serializable {
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 5806542941457644548L;
	private int idLugar;
	private String nombre;
	private String direccion;
	private int capacidad;
	SubEspacio locacion = new SubEspacio(capacidad, direccion);
	
	public EspacioFisico() {}
	
	public EspacioFisico(int idLugar, String direccion, String nombre, int capacidad, SubEspacio locacion) {
		this.idLugar = idLugar;
		this.nombre = nombre;
		this.direccion = direccion;
		this.capacidad = capacidad;
		this.locacion = locacion;
	}
	
	public int getIdLugar() {
		return idLugar;
	}
	public void setIdLugar(int idLugar) {
		this.idLugar = idLugar;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	public SubEspacio getLocacion() {
		return locacion;
	}
	public void setLocacion(SubEspacio locacion) {
		this.locacion = locacion;
	}
	
	public boolean asignarEspacioFisico(String asignarEspacioFisico) {
		return true;
	}
}

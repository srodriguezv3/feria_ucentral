package com.ucentral.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;


public class Proyecto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4744145417105501991L;
	public int idProyecto;
	public String nombreProyecto;
	public int nota;
	Estudiante integrante = new Estudiante();
	Practica nivelPractica = new Practica();
	EspacioFisico espacio = new EspacioFisico();
	Practica practica = new Practica();
	Usuario usuario = new Usuario();
	DocenteTitular titular = new DocenteTitular();
	
	public Proyecto() {}

	
	
	public Proyecto(int idProyecto, String nombreProyecto, int nota) {
		this.idProyecto = idProyecto;
		this.nombreProyecto = nombreProyecto;
		this.nota = nota;

	}



	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public Estudiante getIntegrante() {
		return integrante;
	}

	public void setIntegrante(Estudiante integrante) {
		this.integrante = integrante;
	}

	public Practica getNivelPractica() {
		return nivelPractica;
	}

	public void setNivelPractica(Practica nivelPractica) {
		this.nivelPractica = nivelPractica;
	}

	public EspacioFisico getEspacio() {
		return espacio;
	}

	public void setEspacio(EspacioFisico espacio) {
		this.espacio = espacio;
	}

	public Practica getPractica() {
		return practica;
	}

	public void setPractica(Practica practica) {
		this.practica = practica;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public DocenteTitular getTitular() {
		return titular;
	}

	public void setTitular(DocenteTitular titular) {
		this.titular = titular;
	}
	
	
	



	

}
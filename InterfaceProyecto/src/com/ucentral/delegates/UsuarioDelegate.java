package com.ucentral.delegates;

import java.io.Serializable;
import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ucentral.model.Proyecto;
import com.ucentral.remotebeans.UsuarioRemote;
import com.ucentral.utility.ClientContext;
import com.ucentral.utility.DCMRemoteName;




public class UsuarioDelegate implements UsuarioRemote, Serializable {

	private UsuarioRemote usuarioRemoteDelegate;
	
	
	
	public UsuarioDelegate() throws NamingException {
		
		InitialContext context = ClientContext.getInitialContext();
		usuarioRemoteDelegate = (UsuarioRemote) context.lookup(DCMRemoteName.JNDI_USUARIO_REMOTE);
	}



	@Override
	public boolean probarEJB(String mensaje) {
		return usuarioRemoteDelegate.probarEJB(mensaje);
	}



	
	@Override
	public boolean modificarProyectos(Proyecto obj) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public boolean eliminarProyectos(Proyecto obj) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public ArrayList<Proyecto> consultarProyectos() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public boolean registrarProyectos(Proyecto obj) {
		// TODO Auto-generated method stub
		return false;
	}

}

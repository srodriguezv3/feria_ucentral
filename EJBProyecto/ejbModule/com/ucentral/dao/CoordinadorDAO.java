package com.ucentral.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucentral.bd.ConexionBD;
import com.ucentral.model.Coordinador;
import com.ucentral.model.Proyecto;
import com.ucentral.model.Usuario;


public class CoordinadorDAO {

	private PreparedStatement objPreparedStatement;
	private ResultSet objResultSet;
	private Connection objConexion;
	
	/**
	 * M�todo que obtiene el listado de todos los proyectoss matriculados.
	 * @return 
	 * @return 
	 * @return 
	 * @return lista de proyectoss
	 * @throws SQLException
	 */

	public boolean autenticacion (String id_estudiante, String contrase�a ) {
		objPreparedStatement =null;
		objResultSet = null;
		
				try {
					String consulta =" SELECT * FROM Academia. where id_ =?";
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
		return false;
	}
	
	public ArrayList<Proyecto> consultarProyectos() throws SQLException {
		
		objPreparedStatement = null;
		objResultSet = null;
		objConexion = null;
		
		ArrayList <Proyecto> lstProyectos = new ArrayList <Proyecto>();
		
		String sqlQuery = "SELECT * FROM feria_ucentral.proyectos";
		
		try {
			
			objConexion = ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlQuery);
			objResultSet = objPreparedStatement.executeQuery();
			
			while (objResultSet.next()) {
				
				Proyecto proyectosBD = new Proyecto(
						);
				
				lstProyectos.add(proyectosBD);
					
			}
			objPreparedStatement.close();
			objConexion.close();
			
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
			
		}finally {
			
			if (objConexion != null)
				objConexion.close();
				
			if (objPreparedStatement != null)
				objPreparedStatement.close();
			
		}
		return lstProyectos;
		
	}
	
	public boolean registrarProyectos (Coordinador objModel) throws SQLException {
		
		objPreparedStatement =  null;
		objResultSet = null;
		objConexion = null;
		int reInsert = 0;
		
		String sqlInsert = "INSERT INTO estudiantes (id_estudiante, nombre_estudiante, "
				+ "email_estudiante, password_estudiante, genero_estudiante, direccion_estudiante) VALUES"
							+"(?, ?, ?, ?, ?, ?)";
		
		try {
			objConexion =ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlInsert);
			
		
			
			
			reInsert = objPreparedStatement.executeUpdate();
			
			
			
			objPreparedStatement.close();
			objConexion.close();
			
			if (reInsert>0) {
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			e.getMessage();
			
		}finally {
			if (objConexion !=null)
				objConexion.close();
			
			if(objPreparedStatement !=null)
				objPreparedStatement.close();
		}
		return false;
	}
	

	public boolean modificarProyectos(Proyecto obj)throws SQLException {
		
		objPreparedStatement =  null;
		objResultSet = null;
		objConexion = null;
		int reModify = 0;
		
		String sqlModify = "UPDATE feria_ucentral.proyectos SET id_proyectos=?"
						+" WHERE id_proyectos = ? ";
		
		try {
			objConexion =ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlModify);
			
		
						
			reModify = objPreparedStatement.executeUpdate();
			
			objPreparedStatement.close();
			objConexion.close();
			
			if (reModify>0) {
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
			
		}finally {
			if (objConexion !=null)
				objConexion.close();
			
			if(objPreparedStatement !=null)
				objPreparedStatement.close();
		}
	}
	
	public boolean eliminarProyectos (Proyecto obj) throws SQLException {
		
		objPreparedStatement =  null;
		objResultSet = null;
		objConexion = null;
		int reDelete = 0;
		
		String sqlDelete = "DELETE FROM feria_ucentral.proyectoss WHERE id_proyectos = ? ";
		
		
		try {
			objConexion =ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlDelete);
			
		
	
						
			reDelete = objPreparedStatement.executeUpdate();
			
			objPreparedStatement.close();
			objConexion.close();
			
			if (reDelete>0) {
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
			
		}finally {
			if (objConexion !=null)
				objConexion.close();
			
			if(objPreparedStatement !=null)
				objPreparedStatement.close();
		
		}
			
	}
public boolean agregarEvaluador (Usuario usuario) throws SQLException {
		
		objPreparedStatement =  null;
		objResultSet = null;
		objConexion = null;
		int reDelete = 0;
		
		String sqlDelete = "DELETE FROM feria_ucentral.proyectoss WHERE id_proyectos = ? ";
		
		
		try {
			objConexion =ConexionBD.obtenerConexionBaseDeDatos();
			objPreparedStatement = objConexion.prepareStatement(sqlDelete);
			
		
	
						
			reDelete = objPreparedStatement.executeUpdate();
			
			objPreparedStatement.close();
			objConexion.close();
			
			if (reDelete>0) {
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
			
		}finally {
			if (objConexion !=null)
				objConexion.close();
			
			if(objPreparedStatement !=null)
				objPreparedStatement.close();
		
		}
			
	}
}
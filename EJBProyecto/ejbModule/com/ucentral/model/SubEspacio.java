package com.ucentral.model;

import java.io.Serializable;

public class SubEspacio implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4798388268672015485L;
	private int idSubespacio; 
	private String nombreSubespacio;
	
	public SubEspacio(int idSubespacio, String nombreSubespacio) {
		this.idSubespacio = idSubespacio;
		this.nombreSubespacio = nombreSubespacio;
	}
	public int getIdSubespacio() {
		return idSubespacio;
	}
	public void setIdSubespacio(int idSubespacio) {
		this.idSubespacio = idSubespacio;
	}
	public String getNombreSubespacio() {
		return nombreSubespacio;
	}
	public void setNombreSubespacio(String nombreSubespacio) {
		this.nombreSubespacio = nombreSubespacio;
	}
	
	public boolean asignarSubespacio(String asignarSubespacio) {
		return true;
	}
}

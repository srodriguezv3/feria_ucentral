package com.ucentral.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucentral.dao.CoordinadorDAO;

public class Coordinador implements Serializable {

		
		/**
	 * 
	 */
	private static final long serialVersionUID = -8527373168309530184L;
		private String idCoordinador;
		private String nombreCoordinador;
		private String correoCoordinador;
		public Coordinador(String idCoordinador, String nombreCoordinador) {
			this.idCoordinador = idCoordinador;
			this.nombreCoordinador = nombreCoordinador;
			
		}
		
		public String getIdCoordinador() {
			return idCoordinador;
		}
		public void setIdCoordinador(String idCoordinador) {
			this.idCoordinador = idCoordinador;
		}
		public String getNombreCoordinador() {
			return nombreCoordinador;
		}
		public void setNombreCoordinador(String nombreCoordinador) {
			this.nombreCoordinador = nombreCoordinador;
		}
		public String getCorreoCoordinador() {
			return correoCoordinador;
		}
		public void setCorreoCoordinador(String correoCoordinador) {
			this.correoCoordinador = correoCoordinador;
		}
		
		
		public ArrayList<Proyecto> consultarProyectos() {
			CoordinadorDAO dao = new CoordinadorDAO();
			try {
				return dao.consultarProyectos();
			}catch (SQLException e) {
				e.printStackTrace();
			return null;
			}
		}

}
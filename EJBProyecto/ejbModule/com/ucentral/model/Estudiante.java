package com.ucentral.model;

import java.io.Serializable;

public class Estudiante implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8675772202538909336L;
	private int idEstudiante;
	private String nombreEstudiante;
	private String correoEstudiante;
	public int getIdEstudiante() {
		return idEstudiante;
	}
	
	public Estudiante() {}
	
	public Estudiante(int idEstudiante, String nombreEstudiante, String correoEstudiante) {
		this.idEstudiante = idEstudiante;
		this.nombreEstudiante = nombreEstudiante;
		this.correoEstudiante = correoEstudiante;
	}



	public void setIdEstudiante(int idEstudiante) {
		this.idEstudiante = idEstudiante;
	}
	public String getNombreEstudiante() {
		return nombreEstudiante;
	}
	public void setNombreEstudiante(String nombreEstudiante) {
		this.nombreEstudiante = nombreEstudiante;
	}
	public String getCorreoEstudiante() {
		return correoEstudiante;
	}
	public void setCorreoEstudiante(String correoEstudiante) {
		this.correoEstudiante = correoEstudiante;
	}
	
	public boolean asignarEstudiante() {
		return false;
		
	}
	
}


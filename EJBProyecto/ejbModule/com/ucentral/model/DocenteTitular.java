package com.ucentral.model;

import java.io.Serializable;

public class DocenteTitular implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5280830387372622825L;
	private  int idProfesor;
	private String contraseñaPro;
	private String nombrePro;
	private String apellidoPro;
	private Boolean cargo;
	
	public DocenteTitular(int idProfesor, String contraseñaPro, String nombrePro, String apellidoPro, Boolean cargo) {
		this.idProfesor = idProfesor;
		this.contraseñaPro = contraseñaPro;
		this.nombrePro = nombrePro;
		this.apellidoPro = apellidoPro;
		this.cargo = cargo;
	}
	public DocenteTitular(){}
	
	public int getIdProfesor() {
		return idProfesor;
	}
	public void setIdProfesor(int idProfesor) {
		this.idProfesor = idProfesor;
	}
	public String getContraseñaPro() {
		return contraseñaPro;
	}
	public void setContraseñaPro(String contraseñaPro) {
		this.contraseñaPro = contraseñaPro;
	}
	public String getNombrePro() {
		return nombrePro;
	}
	public void setNombrePro(String nombrePro) {
		this.nombrePro = nombrePro;
	}
	public String getApellidoPro() {
		return apellidoPro;
	}
	public void setApellidoPro(String apellidoPro) {
		this.apellidoPro = apellidoPro;
	}
	public Boolean getCargo() {
		return cargo;
	}
	public void setCargo(Boolean cargo) {
		this.cargo = cargo;
	}
	
	
}

package com.ucentral.model;

import java.io.Serializable;

public class Practica implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4575139942682589383L;
	private int idPractica;
	private String nombrePractica;
	private int nivelPractica;
	
	public Practica(){}
	
	public Practica(int idPractica, String nombrePractica, int nivelPractica) {
		this.idPractica = idPractica;
		this.nombrePractica = nombrePractica;
		this.nivelPractica = nivelPractica;
	}
	
	public int getIdPractica() {
		return idPractica;
	}
	public void setIdPractica(int idPractica) {
		this.idPractica = idPractica;
	}
	public String getNombrePractica() {
		return nombrePractica;
	}
	public void setNombrePractica(String nombrePractica) {
		this.nombrePractica = nombrePractica;
	}
	public int getNivelPractica() {
		return nivelPractica;
	}
	public void setNivelPractica(int nivelPractica) {
		this.nivelPractica = nivelPractica;
	}
	
	public boolean asignarPractica(String asignarPractica) {
		return true;
	}
	
	
}
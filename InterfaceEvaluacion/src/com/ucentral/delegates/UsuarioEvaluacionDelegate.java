package com.ucentral.delegates;

import java.io.Serializable;
import java.util.ArrayList;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ucentral.model.CriterioEvaluacion;
import com.ucentral.model.ProyectoPrueba;
import com.ucentral.remotebeans.UsuarioEvaluacionRemote;
import com.ucentral.utility.ClientContext;
import com.ucentral.utility.DCMRemoteName;


public class UsuarioEvaluacionDelegate implements UsuarioEvaluacionRemote, Serializable {

	private UsuarioEvaluacionRemote usuarioRemoteDelegate;
	
		
	public UsuarioEvaluacionDelegate() throws NamingException {
		
		InitialContext context = ClientContext.getInitialContext();
		usuarioRemoteDelegate = (UsuarioEvaluacionRemote) context.lookup(DCMRemoteName.JNDI_USUARIO_EVALUACION_REMOTE);
	}

	@Override
	public boolean probarEJB(String mensaje) {
		return usuarioRemoteDelegate.probarEJB(mensaje);
	}

	@Override
	public ArrayList<ProyectoPrueba> consultarProyectosAsignados() {
		
		return usuarioRemoteDelegate.consultarProyectosAsignados();
	}

	@Override
	public boolean calificarProyectos(ProyectoPrueba obj) {
		
		return usuarioRemoteDelegate.calificarProyectos(obj);
	}

	@Override
	public boolean crearCriterioNuevo(CriterioEvaluacion criterio) {
		return usuarioRemoteDelegate.crearCriterioNuevo(criterio);
	}



}


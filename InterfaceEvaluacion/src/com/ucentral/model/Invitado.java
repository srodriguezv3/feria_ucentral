package com.ucentral.model;

import java.io.Serializable;

public class Invitado extends Evaluador implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4378701671506140702L;
			private String idInvitado;
		private String nombreInvitado;
		private String apellidoInvitado;
		private String correoInvitado;
		
			public Invitado(String idInvitado, String nombreInvitado, String apellidoInvitado, String correoInvitado) {
			super();
			this.idInvitado = idInvitado;
			this.nombreInvitado = nombreInvitado;
			this.apellidoInvitado = apellidoInvitado;
			this.correoInvitado = correoInvitado;
			
		}

			public String getIdInvitado() {
			return idInvitado;
		}


		public void setIdInvitado(String idInvitado) {
			this.idInvitado = idInvitado;
		}


		public String getNombreInvitado() {
			return nombreInvitado;
		}


		public void setNombreInvitado(String nombreInvitado) {
			this.nombreInvitado = nombreInvitado;
		}


		public String getApellidoInvitado() {
			return apellidoInvitado;
		}


		public void setApellidoInvitado(String apellidoInvitado) {
			this.apellidoInvitado = apellidoInvitado;
		}


		public String getCorreoInvitado() {
			return correoInvitado;
		}


		public void setCorreoInvitado(String correoInvitado) {
			this.correoInvitado = correoInvitado;
		}
	
	
}


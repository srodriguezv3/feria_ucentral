package com.ucentral.model;

import java.io.Serializable;



public class ProyectoPrueba implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8941277746856843638L;
	private int idProyecto;
	private String nombreProyecto;
	private int nota;
	private String idEvaluador;

	public ProyectoPrueba() {
	};

	public ProyectoPrueba(int idProyecto, String nombreProyecto, int nota, String idEvaluador) {
		super();
		this.idProyecto = idProyecto;
		this.nombreProyecto = nombreProyecto;
		this.nota = nota;
		this.idEvaluador = idEvaluador;
	}
	
	public ProyectoPrueba(int idProyecto, int nota) {
		super();
		this.idProyecto = idProyecto;
		this.nota = nota;
	}

	public int getIdProyecto() {
		return idProyecto;
	}

	public void setIdProyecto(int idProyecto) {
		this.idProyecto = idProyecto;
	}

	public String getNombreProyecto() {
		return nombreProyecto;
	}

	public void setNombreProyecto(String nombreProyecto) {
		this.nombreProyecto = nombreProyecto;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getIdEvaluador() {
		return idEvaluador;
	}

	public void setIdEvaluador(String idEvaluador) {
		this.idEvaluador = idEvaluador;
	}
}

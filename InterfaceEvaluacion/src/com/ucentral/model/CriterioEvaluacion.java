package com.ucentral.model;

import java.io.Serializable;

public class CriterioEvaluacion implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6955516262604810011L;
	private String idCriterioEvaluacion;
	private String descripcionCriterio;
	private double notaCriterio;
	
	public CriterioEvaluacion(String idCriterioEvaluacion, String descripcionCriterio, double notaCriterio) {
		super();
		this.idCriterioEvaluacion = idCriterioEvaluacion;
		this.descripcionCriterio = descripcionCriterio;
		this.notaCriterio = notaCriterio;
	}
	
	public CriterioEvaluacion() {}
	public String getIdCriterioEvaluacion() {
		return idCriterioEvaluacion;
	}

	public void setIdCriterioEvaluacion(String idCriterioEvaluacion) {
		this.idCriterioEvaluacion = idCriterioEvaluacion;
	}

	public String getDescripcionCriterio() {
		return descripcionCriterio;
	}

	public void setDescripcionCriterio(String descripcionCriterio) {
		this.descripcionCriterio = descripcionCriterio;
	}
	
	public void crearCriterioNuevo() {
		
	}

	public double getNotaCriterio() {
		return notaCriterio;
	}

	public void setNotaCriterio(double notaCriterio) {
		this.notaCriterio = notaCriterio;
	}
	
	
	
	
	

}

	
	


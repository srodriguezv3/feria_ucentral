package com.ucentral.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Rubrica implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3831103302708660918L;
		private String idRubrica;
		private ArrayList<CriterioEvaluacion> itemsEvaluacion;

		public Rubrica() {

		}

		public Rubrica(String idRubrica, ArrayList<CriterioEvaluacion> itemsEvaluacion) {
			super();
			this.idRubrica = idRubrica;
			this.itemsEvaluacion = itemsEvaluacion;
		}

		public String getIdRubrica() {
			return idRubrica;
		}

		public void setIdRubrica(String idRubrica) {
			this.idRubrica = idRubrica;
		}

		public ArrayList<CriterioEvaluacion> getItemsEvaluacion() {
			return itemsEvaluacion;
		}

		public void setItemsEvaluacion(ArrayList<CriterioEvaluacion> itemsEvaluacion) {
			this.itemsEvaluacion = itemsEvaluacion;
		}

}

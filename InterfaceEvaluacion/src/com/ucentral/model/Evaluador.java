package com.ucentral.model;

import java.io.Serializable;


public class Evaluador implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8395722954626967687L;
		private String idEvaluador;

		public Evaluador(String id_Evaluador, String contraseņa_Evaluador) {
			super();
			this.idEvaluador = id_Evaluador;
		}

		public Evaluador() {
		}

		public String getIdEvaluador() {
			return idEvaluador;
		}

		public void setIdEvaluador(String id_Evaluador) {
			this.idEvaluador = id_Evaluador;
		}

}

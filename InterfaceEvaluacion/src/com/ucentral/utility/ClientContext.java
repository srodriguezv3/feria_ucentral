package com.ucentral.utility;

import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ClientContext {
	
	public static InitialContext getInitialContext () throws NamingException {
		
		Properties prop = new Properties();
		prop.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
		prop.setProperty("java.naming.factory.url.pkgs","com.sun.enterprise.naming");
		prop.setProperty("java.naming.factory.url.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
		prop.setProperty("org.omg.CORBA.ORBInitialHost","localhost");
		prop.setProperty("org.omg.CORBA.ORBInitialPort", "3700");
		InitialContext ic = new InitialContext(prop);
		
		return ic;
	}

}
